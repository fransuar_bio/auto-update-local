import sys
import string
import os
from os import listdir

from subprocess import Popen

from datetime import date, datetime
from pathlib import Path
import sys
from proteus import Model
from proteus import config as pconfig
from optparse import OptionParser


def main(options):
    config = pconfig.set_trytond(options.database, options.admin, config_file=options.config_file)
    Lang = Model.get('ir.lang')
    (es,) = Lang.find([('code', '=', 'es')])
    es.translatable = True
    es.save()

    attach_files(config, es, options)
    print ("done.")

def attach_files(config, es, options):
    Patient = Model.get('gnuhealth.patient')
    Attachment = Model.get('ir.attachment')
    attachment = Attachment()

    origin = options.origin
    files = os.listdir(origin)
    for file_ in files:
        if not file_.startswith('.'):
            name_file = file_.split('.')[0]
            gender = name_file[-1].lower()
            ref = name_file.split(' ')[0]
            patient= Patient.find([
                ('puid', '=', ref),
                ('name.gender', '=', gender)])
            if patient:
                with open(origin+'/'+file_,"rb") as file_data:
                    data = file_data.read()
                    attachment = Attachment()
                    attachment.resource= patient[0]
                    attachment.data = data
                    if not data:
                        print('archivo vacio', file_)
                    attachment.name = file_
                    #buscamos coincidencias de nombre de archivo
                    similar_file=Attachment.find([('name', '=', file_)])
                    # si no encuentra un archivo similar lo guarda
                    if len(similar_file) == 0:
                        print('se adjunta', file_)
                        attachment.save()
                    # si encuentra uno, comparamos el tamanio del mismo
                    else:
                        data_file = similar_file[0]
                        data_size = bytes(similar_file[0].data_size)
                        # si el archivo a adjuntar es mayor en relacion al que
                        # ya esta adjuntado, lo actualiza
                        if data_size < attachment.data:
                            data_file.name = file_
                            data_file.data = attachment.data
                            data_file.save()

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--database', dest='database')
    parser.add_option('-c', '--config-file', dest='config_file')
    parser.add_option('-a', '--admin', dest='admin')
    parser.add_option('-o', '--origin', dest='origin')
    parser.set_defaults(user='admin')

    options, args = parser.parse_args()
    if args:
        parser.error('Parametros incorrectos')
    if not options.database:
        parser.error('Se debe definir [nombre] de base de datos')
    if not options.config_file:
        parser.error('Se debe definir el path absoluto al archivo de '
            'configuracion de trytond')
    if not options.admin:
        parser.error('Se debe definir el usuario admin')

    main(options)
